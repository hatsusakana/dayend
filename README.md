# dayend
日结定时程序
hatsusakana@gmail.com  

配置文件格式
```
year month day hour min wday|type|cmd
* * * * * *|1|test.exe            #每分钟执行一次
* * * * * *|1|./test                #每分钟执行一次
2020 3 4 11 0 0|1|./test      #2020/3/4 11:0 周日 执行一次
```

type说明
```
1  后台执行cmd
```
